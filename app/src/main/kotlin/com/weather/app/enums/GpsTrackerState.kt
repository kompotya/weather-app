package com.weather.app.enums

enum class GpsTrackerState {
    ON, OFF
}