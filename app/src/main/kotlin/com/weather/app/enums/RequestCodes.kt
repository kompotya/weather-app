package com.weather.app.enums

enum class RequestCodes {
    REQUEST_AUTOCOMPLETE_PLACE;

    operator fun invoke() = ordinal
}
