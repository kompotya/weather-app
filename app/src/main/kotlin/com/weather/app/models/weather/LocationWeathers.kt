package com.weather.app.models.weather

import com.weather.app.models.Model
import kotlinx.android.parcel.Parcelize


interface LocationWeathers : Model<Long> {
    val weathers: List<OpenWeather>?
    val locationName: String?
}

@Parcelize
data class LocationWeathersModel(
    override var id: Long? = null,
    override val weathers: List<OpenWeather>?,
    override val locationName: String? = null
) : LocationWeathers