package com.weather.app.models.weather

import com.weather.app.models.Model
import kotlinx.android.parcel.Parcelize

interface City : Model<Long> {
    val country: String?
    val sunrise: Long?
    val sunset: Long?
    val name: String?
}

@Parcelize
data class CityModel(
    override var id: Long? = null,
    override val country: String? = null,
    override val sunrise: Long? = null,
    override val sunset: Long? = null,
    override val name: String?
) : City