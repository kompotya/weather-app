package com.weather.app.models.weather

import com.weather.app.models.Model
import kotlinx.android.parcel.Parcelize


interface OpenWeather : Model<Long> {
    var weather: List<Weather>?
    var main: WeatherMainInfo?
    var wind: Wind?
    var locationName: String?
    val dateTime: Long?
}

@Parcelize
data class OpenWeatherModel(
    override var id: Long? = null,
    override var weather: List<Weather>? = null,
    override var main: WeatherMainInfo? = null,
    override var wind: Wind? = null,
    override var locationName: String? = null,
    override val dateTime: Long? = null
) : OpenWeather