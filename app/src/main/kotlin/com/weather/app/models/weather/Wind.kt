package com.weather.app.models.weather

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

interface Wind : Parcelable {
    val speed: Float?
    val deg: Int?
}

@Parcelize
data class WindModel(
    override val speed: Float? = null,
    override val deg: Int? = null
) : Wind