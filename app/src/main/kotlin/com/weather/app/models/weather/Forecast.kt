package com.weather.app.models.weather

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

interface Forecast : Parcelable {
    val weatherList: List<OpenWeather>?
    val city: City?
}

@Parcelize
data class ForecastModel(
    override val weatherList: List<OpenWeather>? = null,
    override val city: City? = null
) : Forecast