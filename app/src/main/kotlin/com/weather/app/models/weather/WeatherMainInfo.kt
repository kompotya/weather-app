package com.weather.app.models.weather

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

interface WeatherMainInfo : Parcelable {
    val temp: Int?
    val tempFeelsLike: Int?
    val minTemp: Int?
    val maxTemp: Int?
    val pressure: Float?
    val humidity: Float?
}

@Parcelize
data class WeatherMainInfoModel(
    override val temp: Int? = null,
    override val tempFeelsLike: Int? = null,
    override val minTemp: Int? = null,
    override val maxTemp: Int? = null,
    override val pressure: Float? = null,
    override val humidity: Float? = null
) : WeatherMainInfo