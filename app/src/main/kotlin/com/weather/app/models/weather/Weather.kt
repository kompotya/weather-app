package com.weather.app.models.weather

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

interface Weather : Parcelable {
    val main: String?
    val description: String?
    val icon: String?
}

@Parcelize
data class WeatherModel(
    override val main: String? = null,
    override val description: String? = null,
    override val icon: String? = null
) : Weather