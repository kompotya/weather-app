package com.weather.app.models

import android.os.Parcelable

interface Model<T> : Parcelable {
    var id: T?
}