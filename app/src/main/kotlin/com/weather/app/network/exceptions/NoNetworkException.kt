package com.weather.app.network.exceptions

class NoNetworkException : Exception() {

    companion object {
        //TODO: change error code
        private val ERROR_MESSAGE = "No internet connection"
    }

    override val message: String = ERROR_MESSAGE
}
