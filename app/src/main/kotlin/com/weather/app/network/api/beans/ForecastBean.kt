package com.weather.app.network.api.beans

import com.fasterxml.jackson.annotation.JsonProperty

data class ForecastBean(
    @JsonProperty("list")
    val weatherForecast: List<OpenWeatherBean>? = null,
    @JsonProperty("city")
    val cityBean: CityBean? = null
)
