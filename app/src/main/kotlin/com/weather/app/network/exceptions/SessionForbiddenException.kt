package com.weather.app.network.exceptions


class SessionForbiddenException : Exception() {

    companion object {
        //TODO: change error code
        private val ERROR_MESSAGE = "Server error"
    }

    override val message: String = ERROR_MESSAGE
}