package com.weather.app.network

import com.weather.app.network.api.retrofitApi.OpenWeatherApi
import com.weather.app.network.modules.weather.WeatherModule
import com.weather.app.network.modules.weather.WeatherModuleImpl


object NetworkModuleInjector {

    private var weatherModule: WeatherModule? = null

    fun getWeatherModule(): WeatherModule =
        weatherModule
            ?: WeatherModuleImpl(NetworkModule.client.retrofit.create(OpenWeatherApi::class.java))
                .apply { weatherModule = this }
}