package com.weather.app.network.api.converters

import com.weather.app.models.converters.BaseConverter
import com.weather.app.models.weather.City
import com.weather.app.models.weather.CityModel
import com.weather.app.network.api.beans.CityBean

class CityBeanConverter : BaseConverter<CityBean, City>() {

    override fun processConvertInToOut(inObject: CityBean?) = inObject?.run {
        CityModel(id, country, sunrise, sunset, name)
    }

    override fun processConvertOutToIn(outObject: City?) = outObject?.run {
        CityBean(id, country, sunrise, sunset, name)
    }
}
