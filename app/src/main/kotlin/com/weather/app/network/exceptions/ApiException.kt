package com.weather.app.network.exceptions


/**
 * Error from server.
 */
open class ApiException : Exception {

    open var statusCode: Int? = null
    var showMessage: String? = null
        get() = field.takeUnless { it.isNullOrEmpty() } ?: super.message

    constructor() : super()

    constructor(
        statusCode: Int?,
        message: String?
    ) : super(message) {
        this.statusCode = statusCode
        this.showMessage = message
    }

    override val message
        get() = showMessage
}