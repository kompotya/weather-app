package com.weather.app.network.modules.weather

import com.weather.app.network.api.beans.ForecastBean
import com.weather.app.network.api.retrofitApi.OpenWeatherApi
import com.weather.app.network.modules.BaseRxModule
import com.weather.app.network.modules.NetworkErrorUtils
import io.reactivex.Single

class WeatherModuleImpl(api: OpenWeatherApi) :
    BaseRxModule<OpenWeatherApi>(api), WeatherModule {

    override fun getWeatherByLatLng(lat: Double?, lng: Double?, apiKey: String?) =
        api.getWeatherByLatLng(lat, lng, apiKey)
            .onErrorResumeNext(NetworkErrorUtils.rxParseSingleError())

    override fun getWeatherForecast(id: Long?, apiKey: String?): Single<ForecastBean> =
        api.getWeatherForecast(id, apiKey)
            .onErrorResumeNext(NetworkErrorUtils.rxParseSingleError())
}
