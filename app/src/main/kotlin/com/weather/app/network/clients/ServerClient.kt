package com.weather.app.network.clients

import com.ihsanbal.logging.Level
import com.ihsanbal.logging.LoggingInterceptor
import com.weather.app.BuildConfig
import com.weather.app.network.NetworkModule.mapper
import okhttp3.OkHttpClient
import okhttp3.internal.platform.Platform
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import java.util.concurrent.TimeUnit

class ServerClient {

    companion object {

        private const val SERVER_TIMEOUT_IN_SECONDS = 30L
    }

    val retrofit: Retrofit = Retrofit.Builder()
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(JacksonConverterFactory.create(mapper))
        .baseUrl(createApiEndpoint())
        .client(createHttpClient())
        .build()

    private fun log() = LoggingInterceptor.Builder()
        .loggable(BuildConfig.DEBUG)
        .setLevel(Level.BASIC)
        .log(Platform.INFO)
        .request("Request>>>>")
        .response("Response<<<<")
        .build()

    private fun createHttpClient(): OkHttpClient = OkHttpClient.Builder()
        .connectTimeout(SERVER_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
        .readTimeout(SERVER_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
        .writeTimeout(SERVER_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
        .apply {
            if (BuildConfig.DEBUG) addInterceptor(log())
        }.build()


    private fun createApiEndpoint() = "${BuildConfig.ENDPOINT}/"

}
