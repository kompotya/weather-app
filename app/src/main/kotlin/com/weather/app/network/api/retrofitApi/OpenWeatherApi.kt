package com.weather.app.network.api.retrofitApi

import com.weather.app.network.API_VERSION
import com.weather.app.network.api.beans.ForecastBean
import com.weather.app.network.api.beans.OpenWeatherBean
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface OpenWeatherApi {

    @GET("$API_VERSION/weather")
    fun getWeatherByLatLng(
        @Query("lat") lat: Double?,
        @Query("lon") lng: Double?,
        @Query("appid") key: String?
    ): Single<OpenWeatherBean>


    @GET("$API_VERSION/forecast")
    fun getWeatherForecast(
        @Query("id") id: Long?,
        @Query("appid") key: String?
    ): Single<ForecastBean>
}
