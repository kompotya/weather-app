package com.weather.app.network.api.converters

import com.weather.app.models.converters.BaseConverter
import com.weather.app.models.weather.Weather
import com.weather.app.models.weather.WeatherModel
import com.weather.app.network.api.beans.WeatherBean

class WeatherBeanConverter : BaseConverter<WeatherBean, Weather>() {

    override fun processConvertInToOut(inObject: WeatherBean?) = inObject?.run {
        WeatherModel(main, description, icon)
    }

    override fun processConvertOutToIn(outObject: Weather?) = outObject?.run {
        WeatherBean(main, description, icon)
    }
}
