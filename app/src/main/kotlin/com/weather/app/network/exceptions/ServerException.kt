package com.weather.app.network.exceptions


class ServerException(
    statusCode: Int? = null,
    message: String? = null
) : ApiException(statusCode, message) {

    companion object {
        //TODO: change error code
        private val ERROR_MESSAGE = "Server error"
        private const val STATUS_CODE = 500
    }

    override val message: String = ERROR_MESSAGE
    override var statusCode: Int? = STATUS_CODE
}
