package com.weather.app.network.api.converters

import com.weather.app.models.converters.BaseConverter
import com.weather.app.models.weather.Wind
import com.weather.app.models.weather.WindModel
import com.weather.app.network.api.beans.WindBean

class WindBeanConverter : BaseConverter<WindBean, Wind>() {

    override fun processConvertInToOut(inObject: WindBean?) = inObject?.run {
        WindModel(speed, deg)
    }

    override fun processConvertOutToIn(outObject: Wind?) = outObject?.run {
        WindBean(speed, deg)
    }
}
