package com.weather.app.network.api.beans

import com.fasterxml.jackson.annotation.JsonProperty

data class CityBean(
    @JsonProperty("id")
    val id: Long?,
    @JsonProperty("country")
    val country: String?,
    @JsonProperty("sunrise")
    val sunrise: Long?,
    @JsonProperty("sunset")
    val sunset: Long?,
    @JsonProperty("name")
    val name: String?
)