package com.weather.app.network.api.converters

import com.weather.app.models.converters.BaseConverter
import com.weather.app.models.weather.Forecast
import com.weather.app.models.weather.ForecastModel
import com.weather.app.network.api.beans.ForecastBean

class ForecastBeanConverter : BaseConverter<ForecastBean, Forecast>() {

    private val cityBeanConverter = CityBeanConverter()
    private val openWeatherBeanConverter = OpenWeatherBeanConverter()

    override fun processConvertInToOut(inObject: ForecastBean?) = inObject?.run {
        ForecastModel(
            openWeatherBeanConverter.listInToOut(weatherForecast),
            cityBeanConverter.inToOut(cityBean)
        )
    }

    override fun processConvertOutToIn(outObject: Forecast?) = outObject?.run {
        ForecastBean(
            openWeatherBeanConverter.listOutToIn(weatherList),
            cityBeanConverter.outToIn(city)
        )
    }
}
