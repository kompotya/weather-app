package com.weather.app.network.modules.weather

import com.weather.app.network.api.beans.ForecastBean
import com.weather.app.network.api.beans.OpenWeatherBean
import io.reactivex.Single

interface WeatherModule {

    fun getWeatherByLatLng(
        lat: Double?,
        lng: Double?,
        apiKey: String?
    ): Single<OpenWeatherBean>

    fun getWeatherForecast(
        id: Long?,
        apiKey: String?
    ): Single<ForecastBean>
}