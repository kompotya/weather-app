package com.weather.app.network.api.converters

import com.weather.app.models.converters.BaseConverter
import com.weather.app.models.weather.OpenWeather
import com.weather.app.models.weather.OpenWeatherModel
import com.weather.app.network.api.beans.OpenWeatherBean

class OpenWeatherBeanConverter : BaseConverter<OpenWeatherBean, OpenWeather>() {

    private val weatherMainBeanConverter = WeatherMainInfoBeanConverter()
    private val weatherBeanConverter = WeatherBeanConverter()
    private val windBeanConverter = WindBeanConverter()

    override fun processConvertInToOut(inObject: OpenWeatherBean?) = inObject?.run {
        OpenWeatherModel(
            id,
            weatherBeanConverter.listInToOut(weather),
            weatherMainBeanConverter.inToOut(main),
            windBeanConverter.inToOut(wind),
            locationName,
            dateTime
        )
    }

    override fun processConvertOutToIn(outObject: OpenWeather?) = outObject?.run {
        OpenWeatherBean()
    }
}
