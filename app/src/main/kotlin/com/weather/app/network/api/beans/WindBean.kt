package com.weather.app.network.api.beans

import com.fasterxml.jackson.annotation.JsonProperty

data class WindBean(
    @JsonProperty("speed")
    val speed: Float?,
    @JsonProperty("deg")
    val deg: Int?
)
