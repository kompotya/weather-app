package com.weather.app.network.modules

abstract class BaseRxModule<T>(val api: T)