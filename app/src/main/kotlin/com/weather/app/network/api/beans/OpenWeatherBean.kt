package com.weather.app.network.api.beans

import com.fasterxml.jackson.annotation.JsonProperty

data class OpenWeatherBean(
    @JsonProperty("weather")
    val weather: List<WeatherBean>? = null,
    @JsonProperty("main")
    val main: WeatherMainInfoBean? = null,
    @JsonProperty("wind")
    val wind: WindBean? = null,
    @JsonProperty("id")
    val id: Long? = null,
    @JsonProperty("dt")
    val dateTime: Long? = null,
    @JsonProperty("name")
    val locationName: String? = null
)
