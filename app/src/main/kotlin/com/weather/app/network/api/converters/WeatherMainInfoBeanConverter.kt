package com.weather.app.network.api.converters

import com.weather.app.extensions.kelvinToCelsius
import com.weather.app.models.converters.BaseConverter
import com.weather.app.models.weather.WeatherMainInfo
import com.weather.app.models.weather.WeatherMainInfoModel
import com.weather.app.network.api.beans.WeatherMainInfoBean

class WeatherMainInfoBeanConverter : BaseConverter<WeatherMainInfoBean, WeatherMainInfo>() {

    override fun processConvertInToOut(inObject: WeatherMainInfoBean?) = inObject?.run {
        WeatherMainInfoModel(
            temp?.kelvinToCelsius()?.toInt(),
            tempFeelsLike?.kelvinToCelsius()?.toInt(),
            minTemp?.kelvinToCelsius()?.toInt(),
            maxTemp?.kelvinToCelsius()?.toInt(), pressure, humidity
        )
    }

    override fun processConvertOutToIn(outObject: WeatherMainInfo?) = outObject?.run {
        WeatherMainInfoBean(
            temp?.toFloat(),
            tempFeelsLike?.toFloat(),
            minTemp?.toFloat(),
            maxTemp?.toFloat(),
            pressure,
            humidity
        )
    }
}
