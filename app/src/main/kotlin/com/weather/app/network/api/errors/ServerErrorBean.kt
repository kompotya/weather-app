package com.weather.app.network.api.errors

import com.fasterxml.jackson.annotation.JsonProperty


data class ServerErrorBean(
    @JsonProperty("cod")
    val code: Int? = null,
    @JsonProperty("message")
    val message: String? = null
)
