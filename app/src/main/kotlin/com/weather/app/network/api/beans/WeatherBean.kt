package com.weather.app.network.api.beans

import com.fasterxml.jackson.annotation.JsonProperty

data class WeatherBean(
    @JsonProperty("main")
    val main: String?,
    @JsonProperty("description")
    val description: String?,
    @JsonProperty("icon")
    val icon: String?
)
