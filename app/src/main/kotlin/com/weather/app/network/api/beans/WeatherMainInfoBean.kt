package com.weather.app.network.api.beans

import com.fasterxml.jackson.annotation.JsonProperty

data class WeatherMainInfoBean(
    @JsonProperty("temp")
    val temp: Float?,
    @JsonProperty("feels_like")
    val tempFeelsLike: Float?,
    @JsonProperty("temp_min")
    val minTemp: Float?,
    @JsonProperty("temp_max")
    val maxTemp: Float?,
    @JsonProperty("pressure")
    val pressure: Float?,
    @JsonProperty("humidity")
    val humidity: Float?
)
