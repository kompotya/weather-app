package com.weather.app.extensions

import android.os.SystemClock
import android.view.View

private const val DEFAULT_CLICK_DEBOUNCE_TIME = 500L

fun View.clickWithDebounce(debounceTime: Long = DEFAULT_CLICK_DEBOUNCE_TIME, action: () -> Unit) {
    setOnClickListener(object : View.OnClickListener {
        private var lastClickTime = 0L
        override fun onClick(v: View) {
            SystemClock.elapsedRealtime().takeIf { it - lastClickTime > debounceTime }
                ?.run {
                    action()
                    lastClickTime = this
                }
        }
    })
}

fun View.hide(gone: Boolean = true) {
    visibility = if (gone) View.GONE else View.INVISIBLE
}

fun View.show() {
    visibility = View.VISIBLE
}

fun View?.setVisibility(isVisible: Boolean, gone: Boolean = true) =
    this?.let { if (isVisible) show() else hide(gone) }