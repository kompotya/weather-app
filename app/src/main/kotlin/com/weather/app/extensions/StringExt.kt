package com.weather.app.extensions

private const val IMAGE_BASE_URL = "http://openweathermap.org/img/wn/"
private const val IMAGE_RESOLUTION = "@2x.png"

fun String.iconFullUrl() = IMAGE_BASE_URL.plus(this).plus(IMAGE_RESOLUTION)

val String.Companion.EMPTY: String
    get() = ""