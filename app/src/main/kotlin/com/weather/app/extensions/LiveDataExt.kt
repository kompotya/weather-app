package com.weather.app.extensions

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

fun <T> LiveData<T>.safeObserveLet(owner: LifecycleOwner, observer: (t: T) -> Unit) {
    this.observe(owner, Observer { it?.let(observer) })
}

fun <T> MutableLiveData<T>.safeSingleObserveLet(owner: LifecycleOwner, observer: (t: T) -> Unit) {
    this.observe(owner, Observer { data ->
        data?.let {
            observer(it)
            this.value = null
        }
    })
}