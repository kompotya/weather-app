package com.weather.app.extensions

fun <T1 : Any, T2 : Any, R : Any> safeLet(
    p1: T1? = null,
    p2: T2? = null,
    block: (T1, T2) -> R?
): R? =
    p1?.let { param1 ->
        p2?.let { block(param1, it) }
    }