package com.weather.app.extensions

private const val KELVIN_BASE_VALUE = 273.15F

fun Float.kelvinToCelsius() = (this - KELVIN_BASE_VALUE)