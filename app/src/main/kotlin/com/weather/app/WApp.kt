package com.weather.app

import android.app.Application
import android.content.Context
import android.os.Build
import androidx.multidex.MultiDex
import com.facebook.stetho.Stetho
import com.google.android.libraries.places.api.Places
import com.weather.app.database.DatabaseCreator
import com.weather.app.utils.NotificationUtils


class WApp : Application() {

    companion object {
        lateinit var instance: WApp
            private set
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(base)
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        DatabaseCreator.createDb(this)
        if (BuildConfig.DEBUG) Stetho.initializeWithDefaults(this)
        if (!Places.isInitialized()) {
            Places.initialize(applicationContext, getString(R.string.key_google_api))
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationUtils(this).createNotificationChannel()
        }
    }
}
