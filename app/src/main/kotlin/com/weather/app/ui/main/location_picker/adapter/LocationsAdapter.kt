package com.weather.app.ui.main.location_picker.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import com.weather.app.R
import com.weather.app.models.weather.LocationWeathers
import com.weather.app.ui.base.BaseRecyclerViewAdapter
import java.lang.ref.WeakReference

class LocationsAdapter(context: Context, listener: OnLocationClickListener) :
    BaseRecyclerViewAdapter<LocationWeathers, LocationsVH>(context),
    OnLocationClickListener {

    private val weakListener = WeakReference<OnLocationClickListener>(listener)

    override fun onBindViewHolder(holder: LocationsVH, position: Int) = holder.bind(data[position])

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationsVH =
        LocationsVH.newInstance(
            LayoutInflater.from(parent.context),
            parent,
            R.layout.item_recent_location,
            this
        )

    override fun onLocationClick(location: LocationWeathers) {
        weakListener.get()?.onLocationClick(location)
    }
}