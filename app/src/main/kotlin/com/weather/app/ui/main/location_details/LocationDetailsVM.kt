package com.weather.app.ui.main.location_details

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.weather.app.extensions.workAsync
import com.weather.app.models.weather.Forecast
import com.weather.app.providers.ProviderInjector
import com.weather.app.ui.base.BaseVM

class LocationDetailsVM(app: Application) : BaseVM(app) {

    val weatherForecastLD = MutableLiveData<Forecast>()
    val deleteLocationLD = MutableLiveData<Long>()

    private val weatherProvider by lazy { ProviderInjector.getWeatherProvider() }

    fun loadWeatherDetails(id: Long?, apiKey: String) {
        showProgress()
        weatherProvider.getWeatherForecast(id, apiKey)
            .workAsync()
            .doOnEvent { _, _ -> hideProgress() }
            .subscribe({
                weatherForecastLD.value = it
            }, {
                onErrorConsumer.accept(it)
            }).addSubscription()
    }

    fun deleteCachedLocation(locationId: Long) {
        weatherProvider.deleteLocationById(locationId)
            .workAsync()
            .subscribe({
                deleteLocationLD.value = it
            }, {
                onErrorConsumer.accept(it)
            }).addSubscription()
    }
}