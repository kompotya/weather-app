package com.weather.app.ui.base

interface BaseErrorView {

    fun onError(error: Any)
}