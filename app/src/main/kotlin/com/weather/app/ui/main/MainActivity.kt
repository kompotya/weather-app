package com.weather.app.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.weather.app.R
import com.weather.app.ui.base.BaseActivity
import com.weather.app.ui.main.location_details.LocationDetailsFragment
import com.weather.app.ui.main.location_picker.LocationFragment
import com.weather.app.ui.main.location_picker.LocationFragmentCallback

class MainActivity : BaseActivity<MainVM>(), LocationFragmentCallback {

    companion object {

        fun start(context: Context?) {
            context?.apply ctx@{
                startActivity(
                    getIntent(
                        this@ctx
                    )
                )
            }
        }

        private fun getIntent(context: Context?) = Intent(context, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        }
    }

    override val viewModelClass = MainVM::class.java

    override val containerId: Int = R.id.flContainer

    override val layoutId = R.layout.activity_main

    override fun getProgressBarId(): Int = R.id.progressView

    override fun hasProgressBar() = true

    override fun observeLiveData() = Unit

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState ?: openLocationFragment()
    }

    override fun openWeatherDetails(locationId: Long, locationName: String) =
        replaceFragment(LocationDetailsFragment.newInstance(locationId, locationName))

    private fun openLocationFragment() =
        replaceFragment(LocationFragment.newInstance(), false)
}
