package com.weather.app.ui.main.location_details

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.weather.app.BuildConfig
import com.weather.app.R
import com.weather.app.extensions.*
import com.weather.app.models.weather.Forecast
import com.weather.app.models.weather.OpenWeather
import com.weather.app.ui.NO_ID
import com.weather.app.ui.base.BaseFragment
import com.weather.app.ui.main.location_details.adapter.OnWeatherClickListener
import com.weather.app.ui.main.location_details.adapter.WeatherAdapter
import kotlinx.android.synthetic.main.fragment_location_details.*
import kotlinx.android.synthetic.main.include_weather_details.*


class LocationDetailsFragment : BaseFragment<LocationDetailsVM>(), OnWeatherClickListener {

    companion object {
        private const val EXTRA_LOCATION_ID = "EXTRA_LOCATION_ID"
        private const val EXTRA_LOCATION_NAME = "EXTRA_LOCATION_NAME"

        fun newInstance(locationId: Long, locationName: String) = LocationDetailsFragment().apply {
            arguments = Bundle().apply {
                putLong(EXTRA_LOCATION_ID, locationId)
                putString(EXTRA_LOCATION_NAME, locationName)
            }
        }
    }

    override val viewModelClass = LocationDetailsVM::class.java

    override val layoutId = R.layout.fragment_location_details

    private val locationId by lazy {
        arguments?.takeIf { it.containsKey(EXTRA_LOCATION_ID) }
            ?.getLong(EXTRA_LOCATION_ID)
    }

    private val locationName by lazy {
        arguments?.takeIf { it.containsKey(EXTRA_LOCATION_NAME) }
            ?.getString(EXTRA_LOCATION_NAME)
    }

    override fun getScreenTitle() = NO_ID
    override fun hasToolbar() = true
    override fun getToolbarId() = R.id.toolbar
    override fun getStringScreenTitle() = locationName ?: String.EMPTY
    override fun needToShowBackNav() = true

    private var weatherAdapter: WeatherAdapter? = null
        get() = field ?: context?.let {
            WeatherAdapter(it, this).apply { field = this }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_delete, menu)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecycler()
        viewModel.loadWeatherDetails(locationId, BuildConfig.OPEN_WEATHER_KEY)
    }

    override fun observeLiveData() {
        viewModel.apply {
            weatherForecastLD.safeSingleObserveLet(this@LocationDetailsFragment) {
                showForecast(it)
            }
            deleteLocationLD.safeSingleObserveLet(this@LocationDetailsFragment) {
                backPressed()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.iDelete -> true.apply { deleteLocation() }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onWeatherClick(weather: OpenWeather) = setupOpenWeather(weather)

    private fun showForecast(forecast: Forecast) {
        with(forecast) {
            tvLocation.text = listOfNotNull(city?.country, city?.name).joinToString()
            weatherList?.let { weatherAdapter?.updateAllNotify(it) }
            weatherList?.firstOrNull()?.let { setupOpenWeather(it) }
        }
    }

    private fun setupOpenWeather(openWeather: OpenWeather) {
        llCurrentWeather.show()
        openWeather.apply {
            weather?.firstOrNull()?.let {
                tvDescription.text = listOfNotNull(it.main, it.description).joinToString()
                ivWeather.loadCircularImage(it.icon?.iconFullUrl())
            }
            main?.apply maininfo@{
                tvCurrentTemp.text = temp?.toString()
                tvFeelsLike.text = tempFeelsLike?.toString()
                tvMaxTemp.text = maxTemp?.toString()
                tvMinTemp.text = minTemp?.toString()
                tvPressure.text = pressure?.toString()
                tvHumidity.text = humidity?.toString()
            }
            tvWindSpeed.text = wind?.speed?.toString()
        }
    }

    private fun setupRecycler() {
        rvForecast.initRecyclerView(weatherAdapter, RecyclerView.HORIZONTAL)
    }

    private fun deleteLocation() {
        locationId?.let {
            viewModel.deleteCachedLocation(it)
        }
    }
}