package com.weather.app.ui.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.weather.app.extensions.EMPTY
import com.weather.app.network.exceptions.ApiException
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer

abstract class BaseVM(application: Application) : AndroidViewModel(application) {

    val errorLD = MutableLiveData<Any>()
    val isLoadingLD = MediatorLiveData<Boolean>()

    protected open val onErrorConsumer = Consumer<Throwable> {
        val errorString = parseApiException(it)
        errorLD.value = if (errorString.isNotEmpty()) errorString else it.message
    }

    private var compositeDisposable: CompositeDisposable? = null

    override fun onCleared() {
        clearSubscription()
        super.onCleared()
    }

    @Suppress("unused")
    protected fun hideProgress() {
        isLoadingLD.value = false
    }

    @Suppress("unused")
    protected fun showProgress() {
        isLoadingLD.value = true
    }

    protected open fun Disposable.addSubscription() = addBackgroundSubscription(this)

    protected open fun parseApiException(throwable: Throwable) =
        (throwable as? ApiException)?.showMessage ?: String.EMPTY

    private fun clearSubscription() {
        compositeDisposable?.apply {
            clear()
            compositeDisposable = null
        }
    }

    private fun addBackgroundSubscription(subscription: Disposable) {
        compositeDisposable?.apply {
            add(subscription)
        } ?: let {
            compositeDisposable = CompositeDisposable()
            compositeDisposable?.add(subscription)
        }
    }
}