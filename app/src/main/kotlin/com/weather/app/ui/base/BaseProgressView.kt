package com.weather.app.ui.base

interface BaseProgressView {

    fun showProgress()

    fun hideProgress()
}