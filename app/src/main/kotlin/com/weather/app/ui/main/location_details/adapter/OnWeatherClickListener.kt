package com.weather.app.ui.main.location_details.adapter

import com.weather.app.models.weather.OpenWeather

interface OnWeatherClickListener {

    fun onWeatherClick(weather: OpenWeather)
}
