package com.weather.app.ui.main.location_picker.adapter


import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.weather.app.R
import com.weather.app.extensions.clickWithDebounce
import com.weather.app.extensions.iconFullUrl
import com.weather.app.extensions.loadCircularImage
import com.weather.app.models.weather.LocationWeathers

class LocationsVH(
    itemView: View,
    private val listener: OnLocationClickListener
) : RecyclerView.ViewHolder(itemView) {

    companion object {
        private const val SLASH_DIVIDER = "/"

        fun newInstance(
            inflater: LayoutInflater,
            parent: ViewGroup?,
            layout: Int,
            listener: OnLocationClickListener
        ) =
            LocationsVH(inflater.inflate(layout, parent, false), listener)
    }

    private val ivWeather = itemView.findViewById<ImageView>(R.id.ivWeather)
    private val tvLocationName = itemView.findViewById<TextView>(R.id.tvLocationName)
    private val tvMinMaxTemp = itemView.findViewById<TextView>(R.id.tvMinMaxTemp)

    @SuppressLint("SetTextI18n")
    fun bind(location: LocationWeathers) {
        with(location) {
            tvLocationName.text = locationName
            weathers?.firstOrNull()?.let {
                tvMinMaxTemp.text = "${it.main?.minTemp}$SLASH_DIVIDER${it.main?.maxTemp}"
                ivWeather.loadCircularImage(it.weather?.firstOrNull()?.icon?.iconFullUrl())
            }
        }

        itemView.clickWithDebounce { listener.onLocationClick(location) }
    }
}