package com.weather.app.ui.base


import android.content.Context
import android.view.LayoutInflater
import androidx.recyclerview.widget.RecyclerView

/**
 * Base adapter for recycler view.
 */
abstract class BaseRecyclerViewAdapter<TData,
        TViewHolder : RecyclerView.ViewHolder>(context: Context, data: List<TData> = listOf()) :
    RecyclerView.Adapter<TViewHolder>() {

    protected val context: Context = context.applicationContext
    protected val inflater: LayoutInflater = LayoutInflater.from(context)
    protected val data: MutableList<TData> = data.toMutableList()

    /**
     * @return the list.
     */
    val all: List<TData>
        get() = data

    override fun getItemCount() = data.size

    fun updateAllNotify(newItems: List<TData>) {
        data.clear()
        data.addAll(newItems)
        notifyDataSetChanged()
    }
}
