package com.weather.app.ui.splash

import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.weather.app.extensions.workAsync
import com.weather.app.ui.main.MainActivity
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit

class SplashActivity : AppCompatActivity() {

    companion object {
        private const val SPLASH_DELAY = 1_000L
    }

    private var redirectDisposable: Disposable? = null

    override fun onResume() {
        super.onResume()
        redirectToOtherScreen { openMainScreen() }
    }

    override fun onPause() {
        redirectDisposable?.takeUnless { it.isDisposed }?.dispose()
        super.onPause()
    }

    private fun redirectToOtherScreen(callback: () -> Unit) {
        redirectDisposable = Single.timer(SPLASH_DELAY, TimeUnit.MILLISECONDS)
            .workAsync()
            .subscribe({
                callback()
            }, {
                Log.e(toString(), it.message.toString())
            })
    }

    private fun openMainScreen() {
        MainActivity.start(this)
        finish()
    }
}
