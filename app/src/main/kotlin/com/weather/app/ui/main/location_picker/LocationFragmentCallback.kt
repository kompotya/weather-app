package com.weather.app.ui.main.location_picker

interface LocationFragmentCallback {

    fun openWeatherDetails(locationId: Long, locationName: String)
}