package com.weather.app.ui.main.location_details.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import com.weather.app.R
import com.weather.app.models.weather.OpenWeather
import com.weather.app.ui.base.BaseRecyclerViewAdapter
import java.lang.ref.WeakReference

class WeatherAdapter(context: Context, listener: OnWeatherClickListener) :
    BaseRecyclerViewAdapter<OpenWeather, WeatherVH>(context),
    OnWeatherClickListener {

    private val weakListener = WeakReference<OnWeatherClickListener>(listener)

    override fun onBindViewHolder(holder: WeatherVH, position: Int) = holder.bind(data[position])

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherVH =
        WeatherVH.newInstance(
            LayoutInflater.from(parent.context),
            parent,
            R.layout.item_weather,
            this
        )

    override fun onWeatherClick(weather: OpenWeather) {
        weakListener.get()?.onWeatherClick(weather)
    }
}