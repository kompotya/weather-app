package com.weather.app.ui.base

import androidx.annotation.StringRes

interface BaseView {

    fun showSnackBar(@StringRes res: Int)

    fun showSnackBar(text: String?)

}