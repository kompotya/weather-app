package com.weather.app.ui.main.location_picker.adapter

import com.weather.app.models.weather.LocationWeathers

interface OnLocationClickListener {

    fun onLocationClick(location: LocationWeathers)
}
