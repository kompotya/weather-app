package com.weather.app.ui.main.location_picker

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.weather.app.extensions.workAsync
import com.weather.app.models.weather.LocationWeathers
import com.weather.app.models.weather.OpenWeather
import com.weather.app.providers.ProviderInjector
import com.weather.app.ui.base.BaseVM

class LocationVM(app: Application) : BaseVM(app) {

    val recentRequestsLD = MutableLiveData<List<LocationWeathers>>()
    val weatherLD = MutableLiveData<OpenWeather>()

    private val weatherProvider by lazy { ProviderInjector.getWeatherProvider() }

    fun loadWeatherByLatLng(lat: Double, lng: Double, apiKey: String) {
        showProgress()
        weatherProvider.getWeatherByLatLng(lat, lng, apiKey)
            .workAsync()
            .doOnEvent { _, _ -> hideProgress() }
            .doAfterSuccess { loadRecentRequests() }
            .subscribe({
                weatherLD.value = it
            }, {
                onErrorConsumer.accept(it)
            }).addSubscription()
    }

    fun loadRecentRequests() {
        weatherProvider.getRecentRequests()
            .workAsync()
            .subscribe({
                recentRequestsLD.value = it
            }, {
                onErrorConsumer.accept(it)
            }).addSubscription()
    }
}