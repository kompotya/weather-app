package com.weather.app.ui.main.location_details.adapter


import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.weather.app.R
import com.weather.app.extensions.clickWithDebounce
import com.weather.app.extensions.iconFullUrl
import com.weather.app.extensions.loadCircularImage
import com.weather.app.models.weather.OpenWeather
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import java.util.concurrent.TimeUnit

class WeatherVH(
    itemView: View,
    private val listener: OnWeatherClickListener
) : RecyclerView.ViewHolder(itemView) {

    companion object {
        private const val DATE_FORMAT = "dd MMMM yyyy\nHH:mm"

        fun newInstance(
            inflater: LayoutInflater,
            parent: ViewGroup?,
            layout: Int,
            listener: OnWeatherClickListener
        ) =
            WeatherVH(
                inflater.inflate(
                    layout,
                    parent,
                    false
                ), listener
            )
    }

    private val ivWeather = itemView.findViewById<ImageView>(R.id.ivWeather)
    private val tvDateTime = itemView.findViewById<TextView>(R.id.tvDateTime)
    private val tvTemp = itemView.findViewById<TextView>(R.id.tvTemp)

    @SuppressLint("SetTextI18n")
    fun bind(openWeather: OpenWeather) {
        with(openWeather) {
            tvTemp.text = main?.temp?.toString()
            tvDateTime.text = dateTime?.let {
                DateTime(
                    TimeUnit.SECONDS.toMillis(it), DateTimeZone.UTC
                ).toString(DATE_FORMAT)
            }
            ivWeather.loadCircularImage(weather?.firstOrNull()?.icon?.iconFullUrl())
        }
        itemView.clickWithDebounce { listener.onWeatherClick(openWeather) }
    }
}