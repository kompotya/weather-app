package com.weather.app.ui.main.location_picker

import android.Manifest
import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.location.LocationManager
import android.os.Bundle
import android.os.IBinder
import android.provider.Settings
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.weather.app.BuildConfig
import com.weather.app.R
import com.weather.app.enums.RequestCodes
import com.weather.app.extensions.*
import com.weather.app.gps.GpsTrackerService
import com.weather.app.models.weather.LocationWeathers
import com.weather.app.models.weather.OpenWeather
import com.weather.app.ui.base.BaseFragment
import com.weather.app.ui.main.location_picker.adapter.LocationsAdapter
import com.weather.app.ui.main.location_picker.adapter.OnLocationClickListener
import com.weather.app.utils.NotificationUtils
import com.weather.app.utils.bindInterfaceOrThrow
import kotlinx.android.synthetic.main.fragment_location.*
import kotlinx.android.synthetic.main.include_locations_placeholder.*
import kotlinx.android.synthetic.main.include_weather_details.*


class LocationFragment : BaseFragment<LocationVM>(), ServiceConnection, OnLocationClickListener {

    companion object {

        fun newInstance() = LocationFragment().apply {
            arguments = Bundle.EMPTY
        }
    }

    override val viewModelClass = LocationVM::class.java

    override val layoutId = R.layout.fragment_location

    override fun getScreenTitle() = R.string.app_name
    override fun hasToolbar() = true
    override fun getToolbarId() = R.id.toolbar
    override fun needToShowBackNav() = false

    private var isGpsServiceBind = false

    private var isNeedGetLocation = true

    private var locationAdapter: LocationsAdapter? = null
        get() = field ?: context?.let {
            LocationsAdapter(it, this).apply { field = this }
        }

    private var callback: LocationFragmentCallback? = null

    private val gpsNotification by lazy {
        NotificationUtils(requireContext()).createNotificationForLocation()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callback = bindInterfaceOrThrow<LocationFragmentCallback>(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_add, menu)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecycler()
        requestCurrentLocation()
        viewModel.loadRecentRequests()
    }

    override fun onPause() {
        unbindTrackingService()
        super.onPause()
    }

    override fun onDetach() {
        callback = null
        super.onDetach()
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.iAdd -> true.apply { showPlacePicker() }
        else -> super.onOptionsItemSelected(item)
    }

    override fun observeLiveData() {
        viewModel.apply {
            recentRequestsLD.safeSingleObserveLet(this@LocationFragment) {
                checkNoResults(it)
                locationAdapter?.updateAllNotify(it)
            }
            weatherLD.safeObserveLet(this@LocationFragment) {
                showWeather(it)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RequestCodes.REQUEST_AUTOCOMPLETE_PLACE()) {
            data?.apply {
                when (resultCode) {
                    Activity.RESULT_OK -> onPlacePicked(Autocomplete.getPlaceFromIntent(this))
                    AutocompleteActivity.RESULT_ERROR -> onError(
                        Autocomplete.getStatusFromIntent(
                            this
                        )
                    )
                }
            }
        }
    }

    override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
        isGpsServiceBind = true
        (service as? GpsTrackerService.GpsTrackerBinder)?.getService()?.apply {
            startObserveLocation(this)
            connect()
        }
    }

    override fun onServiceDisconnected(name: ComponentName?) {
        isGpsServiceBind = false
    }

    override fun onLocationClick(location: LocationWeathers) {
        safeLet(location.id, location.locationName) { id, name ->
            callback?.openWeatherDetails(id, name)
        }
    }

    private fun requestCurrentLocation() {
        if (isNeedGetLocation) {
            requestPermission(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                isDeniedCallback = {
                    showSnackBar(R.string.location_permissions_denied)
                },
                isGrantedCallback = {
                    if (isGpsOn()) {
                        bindTrackingService()
                    } else {
                        //Open settings screen to turn on the gps
                        activity?.startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                    }
                })
        }
    }

    private fun bindTrackingService() {
        safeLet(gpsNotification, activity) { notification, act ->
            GpsTrackerService.startBoundTracking(act, this, notification)
        }
    }

    private fun startObserveLocation(service: GpsTrackerService) {
        service.apply {
            locationLiveData.safeSingleObserveLet(this@LocationFragment) {
                isNeedGetLocation = false
                loadWeather(it.latitude, it.longitude)
                unbindTrackingService()
            }
        }
    }

    private fun isGpsOn() =
        (activity?.getSystemService(Context.LOCATION_SERVICE) as? LocationManager)?.run {
            isProviderEnabled(LocationManager.GPS_PROVIDER)
                    || isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        } ?: false

    private fun unbindTrackingService() {
        if (isGpsServiceBind) {
            activity?.unbindService(this)
            isGpsServiceBind = false
        }
    }

    private fun onPlacePicked(place: Place) {
        place.latLng?.let {
            hideKeyboard()
            loadWeather(it.latitude, it.longitude)
        }
    }

    private fun showWeather(openWeather: OpenWeather) {
        llCurrentWeather.show()
        openWeather.apply {
            tvLocation.text = locationName
            weather?.firstOrNull()?.let {
                tvDescription.text = listOfNotNull(it.main, it.description).joinToString()
                ivWeather.loadCircularImage(it.icon?.iconFullUrl())
            }
            main?.apply maininfo@{
                tvCurrentTemp.text = temp?.toString()
                tvFeelsLike.text = tempFeelsLike?.toString()
                tvMaxTemp.text = maxTemp?.toString()
                tvMinTemp.text = minTemp?.toString()
                tvPressure.text = pressure?.toString()
                tvHumidity.text = humidity?.toString()
            }
            tvWindSpeed.text = wind?.speed?.toString()
        }
    }

    private fun setupRecycler() =
        rvLocations.initRecyclerView(locationAdapter, RecyclerView.HORIZONTAL)

    private fun showPlacePicker() {
        context?.let { ctx ->
            Autocomplete.IntentBuilder(
                AutocompleteActivityMode.OVERLAY,
                listOf(Place.Field.LAT_LNG)
            ).build(ctx).apply {
                startActivityForResult(this, RequestCodes.REQUEST_AUTOCOMPLETE_PLACE())
            }
        }
    }

    private fun checkNoResults(locations: List<LocationWeathers>) {
        if (locations.isEmpty()) llCurrentWeather.hide()
        llLocationsPlaceholder.setVisibility(locations.isEmpty())
        rvLocations.setVisibility(locations.isNotEmpty())
    }

    private fun loadWeather(lat: Double, lng: Double) {
        viewModel.loadWeatherByLatLng(
            lat,
            lng,
            BuildConfig.OPEN_WEATHER_KEY
        )
    }
}