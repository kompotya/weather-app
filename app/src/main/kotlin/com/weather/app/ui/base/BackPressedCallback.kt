package com.weather.app.ui.base

interface BackPressedCallback {

    fun backPressed()
}