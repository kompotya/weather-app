package com.weather.app.ui.base

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.weather.app.extensions.hideKeyboard


@Suppress("LeakingThis")
abstract class BaseActivity<T : BaseVM> : AppCompatActivity(),
    BaseView,
    BackPressedCallback,
    BaseErrorView,
    BaseProgressView {

    abstract val viewModelClass: Class<T>

    protected abstract val containerId: Int

    protected abstract val layoutId: Int

    private var vProgress: View? = null

    protected abstract fun getProgressBarId(): Int

    protected open val viewModel: T by lazy(LazyThreadSafetyMode.NONE) {
        ViewModelProvider(this).get(viewModelClass)
    }

    protected open val progressObserver = Observer<Boolean> { isShowProgress ->
        isShowProgress?.let { if (it) showProgress() else hideProgress() }
    }

    protected open val errorObserver = Observer<Any> { error ->
        error?.let { processError(it) }
    }

    abstract fun observeLiveData()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
        if (hasProgressBar()) vProgress = findViewById(getProgressBarId())
        observeAllLiveData()
    }

    override fun onBackPressed() {
        hideKeyboard()
        super.onBackPressed()
    }

    override fun backPressed() {
        with(supportFragmentManager) {
            backStackEntryCount.takeUnless { it == 0 }?.let { popBackStack() } ?: onBackPressed()
        }
    }

    override fun showProgress() {
        vProgress?.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        vProgress?.visibility = View.GONE
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        supportFragmentManager.findFragmentById(containerId)
            ?.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        supportFragmentManager.findFragmentById(containerId)
            ?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onError(error: Any) {
        when (error) {
            is Throwable -> showSnackBar(error.message)
            is String -> showSnackBar(error)
        }
    }

    override fun showSnackBar(text: String?) {
        showSnackBar(findViewById(android.R.id.content), text)
    }

    override fun showSnackBar(res: Int) {
        showSnackBar(findViewById(android.R.id.content), getString(res))
    }

    protected open fun processError(error: Any) = onError(error)

    protected open fun hasProgressBar(): Boolean = false

    protected open fun replaceFragment(fragment: Fragment, needToAddToBackStack: Boolean = true) {
        val name = fragment.javaClass.simpleName
        with(supportFragmentManager.beginTransaction()) {
            replace(containerId, fragment, name)
            if (needToAddToBackStack) {
                addToBackStack(name)
            }
            commit()
        }
    }

    private fun showSnackBar(rootView: View?, text: String?) {
        text?.let { txt ->
            rootView?.let {
                hideKeyboard()
                Snackbar.make(it, txt, Snackbar.LENGTH_LONG)
                    .show()
            }
        }
    }

    private fun observeAllLiveData() {
        observeLiveData()
        with(viewModel) {
            isLoadingLD.observe(this@BaseActivity, progressObserver)
            errorLD.observe(this@BaseActivity, errorObserver)
        }
    }
}