package com.weather.app.ui.base

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tbruyelle.rxpermissions2.RxPermissions
import com.weather.app.extensions.hideKeyboard
import com.weather.app.extensions.workAsync
import com.weather.app.ui.NO_TITLE
import com.weather.app.ui.NO_TOOLBAR
import com.weather.app.utils.bindInterfaceOrThrow
import io.reactivex.disposables.CompositeDisposable


/**
 *  Base fragment that implements work with the ViewModel and the life cycle.
 */
abstract class BaseFragment<T : BaseVM> : Fragment(),
    BaseView,
    BaseErrorView,
    BaseProgressView,
    BackPressedCallback {

    abstract val viewModelClass: Class<T>

    protected open val viewModel: T by lazy(LazyThreadSafetyMode.NONE) {
        ViewModelProvider(this).get(viewModelClass)
    }

    protected abstract val layoutId: Int

    protected var toolbar: Toolbar? = null

    var baseView: BaseView? = null
        private set

    var baseProgressView: BaseProgressView? = null
        private set

    var baseErrorView: BaseErrorView? = null
        private set

    var backPressedCallback: BackPressedCallback? = null
        private set

    protected open val progressObserver = Observer<Boolean> {
        if (it == true) showProgress() else hideProgress()
    }

    protected open val errorObserver = Observer<Any> { error ->
        error?.let { processError(it) }
    }

    private val rxPermission by lazy { RxPermissions(this) }

    private val permissionDisposable = CompositeDisposable()

    abstract fun observeLiveData()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        baseView = bindInterfaceOrThrow<BaseView>(parentFragment, context)
        baseErrorView = bindInterfaceOrThrow<BaseErrorView>(parentFragment, context)
        baseProgressView = bindInterfaceOrThrow<BaseProgressView>(parentFragment, context)
        backPressedCallback = bindInterfaceOrThrow<BackPressedCallback>(parentFragment, context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(layoutId, container, false)
        hideKeyboard(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeAllLiveData()
    }

    override fun onResume() {
        super.onResume()
        initToolbar()
        view?.let { hideKeyboard(it) }
    }

    override fun onDestroyView() {
        permissionDisposable.clear()
        super.onDestroyView()
    }

    override fun onDetach() {
        baseView = null
        super.onDetach()
    }

    override fun showProgress() {
        baseProgressView?.showProgress()
    }

    override fun hideProgress() {
        baseProgressView?.hideProgress()
    }

    override fun showSnackBar(res: Int) {
        baseView?.showSnackBar(res)
    }

    override fun showSnackBar(text: String?) {
        baseView?.showSnackBar(text)
    }

    override fun backPressed() {
        backPressedCallback?.backPressed()
    }

    override fun onError(error: Any) {
        baseErrorView?.onError(error)
    }

    protected open fun hasBottomNavigation() = false

    protected open fun processError(error: Any) = onError(error)

    @StringRes
    protected abstract fun getScreenTitle(): Int

    protected abstract fun hasToolbar(): Boolean

    @IdRes
    protected abstract fun getToolbarId(): Int

    protected open fun handleNavigation() = backPressed()

    protected open fun backNavigationIcon(): Int? = null

    /**
     * Setup action bar
     *
     * @param actionBar Modified action bar
     */
    protected fun setupActionBar(actionBar: ActionBar) {
        actionBar.apply {
            title = getStringScreenTitle()
            setDisplayHomeAsUpEnabled(needToShowBackNav())
        }
    }

    protected open fun needToShowBackNav() = true

    protected open fun getStringScreenTitle() =
        if (getScreenTitle() != NO_TITLE) getString(getScreenTitle()) else ""

    protected fun <T : BaseRecyclerViewAdapter<*, *>> RecyclerView.initRecyclerView(
        adapter: T?,
        @RecyclerView.Orientation orientation: Int = RecyclerView.VERTICAL
    ) {
        layoutManager = LinearLayoutManager(context, orientation, false)
        this.adapter = adapter
    }

    protected open fun requestPermission(
        vararg permission: String,
        isDeniedCallback: () -> Unit = { },
        isGrantedCallback: () -> Unit = { }
    ) {
        rxPermission.request(*permission)
            .workAsync()
            .subscribe({ granted ->
                if (granted) isGrantedCallback() else isDeniedCallback()
            }, {
                Log.e(toString(), it.message.toString())
            })?.let { permissionDisposable.add(it) }
    }

    private fun observeAllLiveData() {
        observeLiveData()
        with(viewModel) {
            isLoadingLD.observe(viewLifecycleOwner, progressObserver)
            errorLD.observe(viewLifecycleOwner, errorObserver)
        }
    }

    private fun initToolbar() {
        view?.apply {
            if (hasToolbar() && getToolbarId() != NO_TOOLBAR) {
                toolbar = findViewById(getToolbarId())
                with(activity as AppCompatActivity) {
                    setSupportActionBar(toolbar)
                    supportActionBar?.let {
                        setupActionBar(it)
                        backNavigationIcon()?.let { toolbar?.setNavigationIcon(it) }
                        if (needToShowBackNav()) {
                            toolbar?.setNavigationOnClickListener { handleNavigation() }
                        }
                    }
                }
            }
        }
    }
}