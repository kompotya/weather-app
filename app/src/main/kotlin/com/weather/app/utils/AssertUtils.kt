package com.weather.app.utils


inline fun <reified T> bindInterfaceOrThrow(vararg objects: Any?): T =
    objects.find { it is T }
        ?.let { it as T }
        ?: throw UnsupportedOperationException(T::class.java.simpleName)