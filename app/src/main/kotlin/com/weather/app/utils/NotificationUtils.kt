package com.weather.app.utils

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build.VERSION.SDK_INT
import android.os.Build.VERSION_CODES.O
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.weather.app.R
import java.lang.ref.WeakReference

class NotificationUtils(context: Context) {

    private val weakContext = WeakReference(context)

    fun createNotificationForLocation(): Notification? =
        weakContext.get()?.applicationContext?.let {
            NotificationCompat.Builder(
                it, it.getString(
                    R.string.app_name
                )
            ).run {
                setOngoing(true)
                setSmallIcon(R.mipmap.ic_launcher)
                priority = NotificationCompat.PRIORITY_MIN
                setContentText(it.getString(R.string.notification_track_location))
                if (SDK_INT >= O) setCategory(Notification.CATEGORY_SERVICE)
                build()
            }
        }

    @RequiresApi(O)
    fun createNotificationChannel() =
        weakContext.get()?.applicationContext?.let {
            it.getString(R.string.app_name).let { channelId ->
                NotificationChannel(
                    channelId,
                    channelId,
                    NotificationManager.IMPORTANCE_HIGH
                ).apply {
                    lockscreenVisibility = Notification.VISIBILITY_PRIVATE
                    (it.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
                        .createNotificationChannel(this)
                }
            }
        }
}
