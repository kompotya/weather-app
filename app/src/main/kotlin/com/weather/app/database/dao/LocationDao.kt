package com.weather.app.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.weather.app.database.LOCATION_TABLE
import com.weather.app.database.tables.LocationDb
import com.weather.app.database.tables.LocationRelation
import io.reactivex.Single

@Dao
interface LocationDao : BaseDao<LocationDb> {

    @Query("SELECT * FROM $LOCATION_TABLE WHERE id = :id")
    fun getLocationById(id: Long): Single<LocationDb>

    @Query("SELECT * FROM $LOCATION_TABLE WHERE location.id = :locationId")
    fun getLocationsWithWeather(locationId: Long): Single<LocationRelation>

    @Query("SELECT * FROM $LOCATION_TABLE")
    fun getAllLocationsWithWeather(): Single<List<LocationRelation>>

    @Query("DELETE FROM $LOCATION_TABLE WHERE id = :id")
    fun deleteLocationById(id: Long)
}
