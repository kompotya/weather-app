package com.weather.app.database.converters

import com.weather.app.database.tables.LocationRelation
import com.weather.app.models.converters.BaseConverter
import com.weather.app.models.weather.LocationWeathersModel

class LocationDbRelationConverter : BaseConverter<LocationRelation, LocationWeathersModel>() {

    private val weatherDbConverter = WeatherDbConverter()

    override fun processConvertInToOut(inObject: LocationRelation?) = inObject?.run {
        LocationWeathersModel(
            this.location?.id,
            this.weather?.mapNotNull {
                weatherDbConverter.inToOut(it)?.apply {
                    locationName = location?.name
                }
            },
            location?.name
        )
    }

    override fun processConvertOutToIn(outObject: LocationWeathersModel?) = outObject?.run {
        LocationRelation()
    }
}
