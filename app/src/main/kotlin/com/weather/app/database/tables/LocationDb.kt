package com.weather.app.database.tables

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.weather.app.database.LOCATION_TABLE

@Entity(tableName = LOCATION_TABLE)
data class LocationDb(
    @PrimaryKey var id: Long? = null,
    var name: String? = null
)
