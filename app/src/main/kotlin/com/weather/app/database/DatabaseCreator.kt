package com.weather.app.database

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.room.Room
import com.weather.app.BuildConfig.DB_NAME
import com.weather.app.extensions.completableToMain
import io.reactivex.Completable
import java.util.concurrent.atomic.AtomicBoolean


object DatabaseCreator {

    private val isDatabaseCreated = MutableLiveData<Boolean>()
    lateinit var database: TestDB
    private val initializing = AtomicBoolean(true)

    @SuppressWarnings("CheckResult")
    fun createDb(context: Context) {
        if (initializing.compareAndSet(true, false).not()) {
            return
        }

        isDatabaseCreated.value = false

        Completable.fromAction {
            database = Room.databaseBuilder(context, TestDB::class.java, DB_NAME).build()
            database.openHelper.setWriteAheadLoggingEnabled(false)
        }
            .completableToMain()
            .subscribe({ isDatabaseCreated.value = true }, { it.printStackTrace() })
    }
}
