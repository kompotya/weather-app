package com.weather.app.database.repositories

import com.weather.app.database.DatabaseCreator
import com.weather.app.database.converters.LocationDbRelationConverter
import com.weather.app.database.converters.WeatherDbConverter
import com.weather.app.database.tables.LocationDb
import com.weather.app.database.tables.WeatherDb
import com.weather.app.models.weather.LocationWeathers
import com.weather.app.models.weather.OpenWeather
import io.reactivex.Single

interface LocationWeatherRepository {

    fun save(openWeather: OpenWeather): Single<OpenWeather>

    fun getWeatherByLocationId(locationId: Long): Single<LocationWeathers>

    fun getAllWeather(): Single<List<LocationWeathers>>

    fun deleteLocationById(id: Long): Single<Long>
}

class LocationWeatherRepositoryImpl : LocationWeatherRepository {

    private val converter = LocationDbRelationConverter()

    private val dao = DatabaseCreator.database.locationDao()

    private val weatherDao = DatabaseCreator.database.weatherDao()

    private val weatherDbConverter = WeatherDbConverter()

    override fun save(openWeather: OpenWeather): Single<OpenWeather> =
        Single.just(openWeather)
            .map { dao.insert(LocationDb(openWeather.id, openWeather.locationName)) }
            .map {
                weatherDao.insert(
                    weatherDbConverter.outToIn(openWeather) ?: WeatherDb()
                )
            }
            .map { openWeather }

    override fun getWeatherByLocationId(locationId: Long): Single<LocationWeathers> =
        dao.getLocationsWithWeather(locationId)
            .compose(converter.single.inToOut())

    override fun getAllWeather(): Single<List<LocationWeathers>> =
        dao.getAllLocationsWithWeather()
            .compose(converter.single.listInToOut())

    override fun deleteLocationById(id: Long): Single<Long> =
        Single.just(id)
            .map {
                it.apply {
                    dao.deleteLocationById(this)
                }
            }
}
