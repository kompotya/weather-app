package com.weather.app.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.weather.app.database.WEATHER_TABLE
import com.weather.app.database.tables.WeatherDb
import io.reactivex.Single

@Dao
interface WeatherDao : BaseDao<WeatherDb> {

    @Query("SELECT * FROM $WEATHER_TABLE")
    fun getWeathers(): Single<List<WeatherDb>>

    @Query("DELETE FROM $WEATHER_TABLE")
    fun deleteWeatherAll()
}
