package com.weather.app.database.tables

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.weather.app.database.WEATHER_TABLE
import org.joda.time.DateTime

@Entity(
    tableName = WEATHER_TABLE, foreignKeys = [ForeignKey(
        entity = LocationDb::class,
        parentColumns = ["id"],
        childColumns = ["locationId"],
        onDelete = ForeignKey.CASCADE
    )]
)
data class WeatherDb(
    @PrimaryKey(autoGenerate = true) var weatherId: Long? = null,
    var locationId: Long? = null,
    var temp: Int? = null,
    var tempFeelsLike: Int? = null,
    var minTemp: Int? = null,
    var maxTemp: Int? = null,
    var pressure: Float? = null,
    var humidity: Float? = null,
    var windSpeed: Float? = null,
    var windDeg: Int? = null,
    var main: String? = null,
    var description: String? = null,
    var icon: String? = null,
    var timeStamp: DateTime? = null
)