package com.weather.app.database

import com.weather.app.BuildConfig

//common
const val DB_VERSION = BuildConfig.DB_VERSION
const val LOCATION_TABLE = "Location"
const val WEATHER_TABLE = "Weather"
