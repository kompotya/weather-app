package com.weather.app.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.weather.app.database.converters.type_converter.DateTypeConverter
import com.weather.app.database.dao.LocationDao
import com.weather.app.database.dao.WeatherDao
import com.weather.app.database.tables.LocationDb
import com.weather.app.database.tables.WeatherDb

@Database(entities = [LocationDb::class, WeatherDb::class], version = DB_VERSION)
@TypeConverters(value = [DateTypeConverter::class])
abstract class TestDB : RoomDatabase() {

    abstract fun locationDao(): LocationDao

    abstract fun weatherDao(): WeatherDao
}
