package com.weather.app.database.converters

import com.weather.app.database.tables.WeatherDb
import com.weather.app.models.converters.BaseConverter
import com.weather.app.models.weather.*
import org.joda.time.DateTime

class WeatherDbConverter : BaseConverter<WeatherDb, OpenWeather>() {

    override fun processConvertInToOut(inObject: WeatherDb?) = inObject?.run {
        OpenWeatherModel(
            weatherId,
            listOf(WeatherModel(main, description, icon)),
            WeatherMainInfoModel(
                temp,
                tempFeelsLike,
                minTemp,
                maxTemp,
                pressure,
                humidity
            ),
            WindModel(windSpeed, windDeg),
            null
        )
    }

    override fun processConvertOutToIn(outObject: OpenWeather?) = outObject?.run {
        val weatherInfo = weather?.firstOrNull()
        WeatherDb(
            id,
            id,
            main?.temp,
            main?.tempFeelsLike,
            main?.minTemp,
            main?.maxTemp,
            main?.pressure,
            main?.humidity,
            wind?.speed,
            wind?.deg,
            weatherInfo?.main,
            weatherInfo?.description,
            weatherInfo?.icon,
            DateTime.now()
        )
    }
}
