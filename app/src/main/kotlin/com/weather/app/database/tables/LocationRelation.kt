package com.weather.app.database.tables

import androidx.room.Embedded
import androidx.room.Relation

data class LocationRelation(
    @Embedded var location: LocationDb? = null,
    @Relation(
        parentColumn = "id", entityColumn = "locationId",
        entity = WeatherDb::class
    )
    var weather: List<WeatherDb>? = listOf()
) {

    fun getWeatherForLocation() = weather?.takeIf { it.isNotEmpty() }
}
