package com.weather.app.database

import com.weather.app.database.repositories.LocationWeatherRepository
import com.weather.app.database.repositories.LocationWeatherRepositoryImpl

object DatabaseModuleInjector {

    private var locationWeatherRepository: LocationWeatherRepository? = null

    fun getLocationWeatherRepository() =
        locationWeatherRepository
            ?: LocationWeatherRepositoryImpl()
                .apply { locationWeatherRepository = this }
}
