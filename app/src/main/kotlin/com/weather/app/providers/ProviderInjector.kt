package com.weather.app.providers


object ProviderInjector {

    private var weatherProvider: WeatherProvider? = null

    fun getWeatherProvider(): WeatherProvider =
        weatherProvider ?: WeatherProviderImpl().apply {
            weatherProvider = this
        }
}
