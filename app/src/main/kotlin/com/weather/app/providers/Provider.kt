package com.weather.app.providers

import com.weather.app.models.Model


interface Provider<AppModelType : Comparable<AppModelType>,
        AppModel : Model<AppModelType>>