package com.weather.app.providers


import com.weather.app.database.DatabaseModuleInjector
import com.weather.app.models.weather.Forecast
import com.weather.app.models.weather.LocationWeathers
import com.weather.app.models.weather.OpenWeather
import com.weather.app.network.NetworkModuleInjector
import com.weather.app.network.api.converters.ForecastBeanConverter
import com.weather.app.network.api.converters.OpenWeatherBeanConverter
import io.reactivex.Single


interface WeatherProvider {

    fun getWeatherByLatLng(
        lat: Double?,
        lng: Double?,
        key: String?
    ): Single<OpenWeather>

    fun getRecentRequests(): Single<List<LocationWeathers>>

    fun getWeatherForecast(id: Long?, apiKey: String?): Single<Forecast>

    fun deleteLocationById(id: Long): Single<Long>
}

class WeatherProviderImpl : Provider<Long, OpenWeather>, WeatherProvider {

    private val networkModule = NetworkModuleInjector.getWeatherModule()

    private val databaseRepository = DatabaseModuleInjector.getLocationWeatherRepository()

    private val converter = OpenWeatherBeanConverter()
    private val forecastConverter = ForecastBeanConverter()

    override fun getWeatherByLatLng(lat: Double?, lng: Double?, key: String?): Single<OpenWeather> =
        networkModule.getWeatherByLatLng(lat, lng, key)
            .compose(converter.single.inToOut())
            .flatMap { databaseRepository.save(it) }

    override fun getWeatherForecast(id: Long?, apiKey: String?): Single<Forecast> =
        networkModule.getWeatherForecast(id, apiKey)
            .compose(forecastConverter.single.inToOut())

    override fun getRecentRequests(): Single<List<LocationWeathers>> =
        databaseRepository.getAllWeather()

    override fun deleteLocationById(id: Long): Single<Long> =
        databaseRepository.deleteLocationById(id)
}